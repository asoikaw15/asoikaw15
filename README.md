### Hi there 👋

I am Kervy Regidor Cantos, currently working as a Web Developer at AME . I am a Full-Stack Javascript developer and love writing clean and maintainable code. Find out more about me & feel free to connect with me here:

[![Linkedin Badge](https://img.shields.io/badge/-kervy-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/kervy-cantos-3a1402240/)](https://www.linkedin.com/in/kervy-cantos-3a1402240/)
[![Gmail Badge](https://img.shields.io/badge/-krteammain@gmail.com-c14438?style=flat-square&logo=Gmail&logoColor=white&link=mailto:krteammain@gmail.com)](mailto:krteammain@gmail.com)
[![Facebook Badge](https://img.shields.io/badge/kervy-cantos-1877F2?style=flat-square&logo=facebook&logoColor=white&link=https://www.facebook.com/kerv.con)](https://www.facebook.com/kerv.con)


## ⚡ Technologies


![JavaScript](https://img.shields.io/badge/-JavaScript-black?style=flat-square&logo=javascript)
![Nodejs](https://img.shields.io/badge/-Nodejs-black?style=flat-square&logo=Node.js)
![React](https://img.shields.io/badge/-React-black?style=flat-square&logo=react)
![TypeScript](https://img.shields.io/badge/-TypeScript-007ACC?style=flat-square&logo=typescript)
![Python](https://img.shields.io/badge/-Python-black?style=flat-square&logo=Python)
![C++](https://img.shields.io/badge/-C++-00599C?style=flat-square&logo=c)
![HTML5](https://img.shields.io/badge/-HTML5-E34F26?style=flat-square&logo=html5&logoColor=white)
![CSS3](https://img.shields.io/badge/-CSS3-1572B6?style=flat-square&logo=css3)
![Bootstrap](https://img.shields.io/badge/-Bootstrap-563D7C?style=flat-square&logo=bootstrap)
![MongoDB](https://img.shields.io/badge/-MongoDB-black?style=flat-square&logo=mongodb)
![Redis](https://img.shields.io/badge/-Redis-black?style=flat-square&logo=Redis)
![GraphQL](https://img.shields.io/badge/-GraphQL-E10098?style=flat-square&logo=graphql)
![Apollo GraphQL](https://img.shields.io/badge/-Apollo%20GraphQL-311C87?style=flat-square&logo=apollo-graphql)
![PostgreSQL](https://img.shields.io/badge/-PostgreSQL-336791?style=flat-square&logo=postgresql)
![MySQL](https://img.shields.io/badge/-MySQL-black?style=flat-square&logo=mysql)
![Heroku](https://img.shields.io/badge/-Heroku-430098?style=flat-square&logo=heroku)
![Amazon AWS](https://img.shields.io/badge/Amazon%20AWS-232F3E?style=flat-square&logo=amazon-aws)
![Git](https://img.shields.io/badge/-Git-black?style=flat-square&logo=git)
![GitHub](https://img.shields.io/badge/-GitHub-181717?style=flat-square&logo=github)
![GitLab](https://img.shields.io/badge/-GitLab-FCA121?style=flat-square&logo=gitlab)
![BitBucket](https://img.shields.io/badge/-BitBucket-darkblue?style=flat-square&logo=bitbucket)
___
## ⚡ Stats
![GitHub stats](https://github-readme-stats.vercel.app/api?username=asoikaw15&show_icons=true&count_private=true&theme=radical)
![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=asoikaw15&hide=TeX&layout=compact)

![Visitor Badge](https://visitor-badge.laobi.icu/badge?page_id=asoikaw15.asoikaw15)

___

## ⚡ Follow me please
[![forthebadge](https://forthebadge.com/images/badges/powered-by-electricity.svg)](https://forthebadge.com)  [![forthebadge](https://forthebadge.com/images/badges/gluten-free.svg)](https://forthebadge.com)  [![forthebadge](https://forthebadge.com/images/badges/60-percent-of-the-time-works-every-time.svg)](https://forthebadge.com)
